#!/usr/bin/env python
# coding: utf-8

import re
import json
import pandas as pd
import geopandas as gpd
import geojson
import requests
from bs4 import BeautifulSoup
from sklearn.neighbors import KDTree
import numpy as np
import scipy
from pathlib import Path
import os
from OSMPythonTools.overpass import Overpass
from math import radians, cos, sin, asin, sqrt
from datetime import datetime, timedelta

mean_earth_radius = 6371.0088

def get_cameras(url: str) -> pd.DataFrame:
    """Get the sous-surveillance cameras
    """
    r = requests.get(url)
    fixed = re.sub(r'(?<!\\)\\(?!["\\/bfnrt]|u[0-9a-fA-F]{4})', r' ', r.text) # remove wrong escaped char
    json_ = json.loads(fixed)
    df = gpd.GeoDataFrame.from_features(json_["features"])
    df.lon = df.lon.astype(float)
    df.lat = df.lat.astype(float)

    #Clean operator name
    df.op_name = df.op_name.str.replace(r"^\?$","", regex=True)
    df.op_name = df.op_name.str.replace(r"^\?\?\?$","", regex=True)
    df.op_name = df.op_name.str.replace("ÿ","")
    df.op_name = df.op_name.str.replace("fédrale","fédérale")
    df.op_name = df.op_name.str.replace("à vérifier","")

    print("Number of cameras: ", len(df))
    return df


def get_infos(id_):
    survey_date = ''
    description = ''
    surveillanceType = ''

    url = 'https://bruxelles.sous-surveillance.net/spip.php?camera'+str(id_)
    dir_ = Path("data/")
    dir_.mkdir(parents=True, exist_ok=True)
    path = Path(str(dir_) +"/"+ id_ + ".json")
    if not path.exists():
        r = requests.get(url)
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, "html.parser")
            survey_date = soup.abbr["title"]
            description = soup.find_all("ul", "camera_infos")[0].find_all("li")[-1].find(text=True, recursive=False).strip()
            surveillanceType = soup.find_all("ul", "camera_infos")[0].find_all("li")[1].find(text=True, recursive=False).strip()

            data = [{
                "survey_date": f"{survey_date}",
                "description": f"{description}",
                "surveillanceType": f"{surveillanceType}"
            }]

            print("not exists")
            with open(path, "w") as f:
                json.dump(data, f)


def load_infos(id_):
    path = Path("data/"+ id_ + ".json")
    if path.exists():
        with open(path, "r") as f:
            file = f.read()
        data = json.loads(file)
        return data[0]['survey_date'], data[0]['description'], data[0]['surveillanceType']
    else:
        return [None, "", None]

def load_survey_date_description(df):
    _ = df["id_camera"].apply(lambda x: get_infos(x))
    df['survey_date'], df['description'], df["surveillance_zone"] = zip(* df["id_camera"].apply(lambda x: load_infos(x)))
    df.survey_date = pd.to_datetime(df.survey_date)
    print(df.survey_date.dt.year.value_counts())
    return df


def get_osm_cameras() -> pd.DataFrame:
    """Get cameras in OSM

       We use man_made=surveillance in place of "surveillance:type"="camera" as some cameras are only tagged with man_made
    """
    overpass = Overpass()
    result = overpass.query("""
    nwr["man_made"="surveillance"];
    out;
    """, timeout=2500)
    dict_ = result.toJSON()['elements']
    df = pd.DataFrame(dict_)
    return df

def get_new_cameras(df: pd.DataFrame):
    """Get cameras not yet in OSM
    """
    osmdf = get_osm_cameras()
    osmtags = osmdf.tags.apply(pd.Series)
    ref_tag = "ref:sous-surveillance_net"
    ids = osmtags[~osmtags[ref_tag] .isna()][ref_tag].values
    df  = df[~df.id_camera.isin(ids)].copy()
    print("Number of cameras: ", len(df))
    return df, osmdf


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the distance between two points on the earth (specified in decimal degrees)
    https://stackoverflow.com/questions/15736995/how-can-i-quickly-estimate-the-distance-between-two-latitude-longitude-points
    """
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula 
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a))
    m = mean_earth_radius * 1000 * c
    return m


def closest_neigbhours(df: pd.DataFrame, osmdf: pd.DataFrame):
    """Calculate the closest neigbours of  the camera from Sous-surveillance
    """
    # initializing the target variables
    df.loc[:, "osm_neighbour"] = -1
    df.loc[:, "neighbour_dist"] = -1

    osm_latlon = osmdf[["lat", "lon"]].dropna().copy(deep=True)

    ssdf = df[["lat", "lon", "id_camera"]]
    for index, row in ssdf.iterrows():
        X = osm_latlon.copy()
        # adding the current point to the list of potential neighbours
        X.loc[len(osm_latlon), :] = row["lat"], row["lon"]

        # The point we look for neighbours
        sample = X.iloc[-1, :].values.reshape(1, -1)

        # Looking for neighbours
        kdt = KDTree(X, leaf_size=30, metric='euclidean')
        res  = kdt.query(sample, k=2, return_distance=True)

        # Getting the id and dist of the closest neighbour
        neighbour_id = res[1][0][1]
        if neighbour_id == len(osm_latlon):
            dist = 0
            neighbour_id = 0
        else:
            dist = haversine(
                df.loc[index,  "lon"],
                df.loc[index,  "lat"],
                osm_latlon.loc[neighbour_id , "lon"],
                osm_latlon.loc[neighbour_id , "lat"]
            )

        df.loc[index,  "osm_neighbour"] = neighbour_id
        df.loc[index,  "neighbour_dist"] = dist
        print("Camera :", row["id_camera"], " - osm neighbour: ", neighbour_id, " - dist: ", dist)

    print(df.neighbour_dist.value_counts(bins=[0,5,20, df.neighbour_dist.max()], sort=False))
    return df


def set_status(df: pd.DataFrame):
    """Set a status depending on the proximity of osm cameras and depending on the survey data
    """
    old_date = datetime.today() - timedelta(days=3650)
    old_date = old_date.astimezone(None)
    df.loc[df.neighbour_dist > 20, "status"] = "TO_IMPORT"
    df.loc[df.survey_date < old_date, "status"] = "FIXME_OLD"   # manual review
    df.loc[(df.neighbour_dist >= 5) & (df.neighbour_dist < 20), "status"] = "FIXME_DUPLI" # if ss node alone, add only id sous-surv to closest osm node
    df.loc[df.neighbour_dist < 5, "status"] = "TO_DELETE"  # add info to osm nodes --> all info
    return df



def getTags(row):
    tags = dict()
    # for every camera
    tags["man_made"] = "surveillance"
    tags["surveillance:type"] = "camera"
    tags["source"] = "sous-surveillance.net"
    tags["survey:date"] = row["survey_date"]
    if row["apparence"] == "dome":
        tags["camera:type"] = row["apparence"]
    elif row["apparence"] == "radar":
        tags["surveillance:type"] = "ALPR"
    elif row["apparence"] in ["boite", "nue", "encastre"]:
#        if row["camera_rotation"]:
#            tags["camera:type"]  = "panning"
#        else:
          tags["camera:type"]  = "fixed"
    if row["apparence"] == "encastre":
            tags["camera:mount"] = "wall"
    if row["direction"] != "0":
        tags["camera:direction"] = row["direction"]
    if row["angle"] != "0":
        tags["camera:angle"] = row["angle"]
#    if row["camera_zoom"]:
#        tags["camera:feature"] = "zoom"
#   if row["camera_rotation"]:
#        if tags["camera:feature"] == "zoom":
#            tags["camera:feature"] =  "zoom, motion"
#        else:
#            tags["camera:feature"] =  "motion"
    if row["op_type"] == "private":
        tags["surveillance"] = "outdoor"
    elif row["op_type"] == "public":
        tags["surveillance"] = "public"

    if row["surveillance_zone"] == "Municipale":
        tags["surveillance:zone"] = "town"
    elif row["surveillance_zone"] == "Traffic":
        tags["surveillance:zone"] = "traffic"
    elif row["surveillance_zone"] == "Parking":
        tags["surveillance:zone"] = "parking"

    if row["op_name"] != "":
        tags["operator"] = row["op_name"]
    tags["ref:sous-surveillance.net"] = row["id_camera"]

    if (len(row["description"]) == 2) and row["description"][-1] == "m":
        tags["heigth"] = row["description"][0]

    if row["status"] == "FIXME_OLD":
        tags["fixme"] = "This may be disused"
    if row["status"] == "FIXME_DUPLI":
        tags["fixme"] = "This may be a duplicated camera" ,

    tags["status"] = row["status"]   # This tag need to be deleted after review in JOSM

    #Non generic translation
    if row["description"] in [" Poteau vert", "Poteau", "Boîtier blanc monté sur éclairage autoroutier"]:
        tags["camera:mount"] = "pole"
    if row["description"] in ["Façade, 2e étage", "Façade, 1er étage"]:
        tags["camera:mount"] = "wall"
    return tags

    #tags["name"] = row["title"]
    #tags["description"] = row["description"]


def df2geojson(df):
    features = []
    insert_features = lambda X: features.append(
            geojson.Feature(
                geometry=geojson.Point((X["lon"], X["lat"])),
                properties=getTags(X)
            )
    )
    df.apply(insert_features, axis=1)
    return geojson.FeatureCollection(features)


def save(df, output_name):
    #fix date
    df.survey_date = df.survey_date.map(
        lambda x: datetime.strftime(x, '%Y-%m-%d') if not pd.isnull(x) else None
    ) #ISO 8601

    json_ = df2geojson(df)
    with open(f'camerasToTreat_{output_name}.geojson', 'w', encoding='utf8') as fp:
        geojson.dump(json_, fp, sort_keys=True, ensure_ascii=True, default=str)
    print(f"Saved to: camerasToTreat_{output_name}.geojson")
