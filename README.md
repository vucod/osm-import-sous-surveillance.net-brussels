# [OSM import] sous-surveillance.net 

This repository contains the workflow to add cameras of sous-surveillance.net in OpenStreetMap

More information can be found here: https://wiki.openstreetmap.org/wiki/Import/Catalogue/sous-surveillance.net


## Requirements

- https://python-poetry.org/

## How to get started

``` 
poetry install
poetry shell
jupyter-notebook
```
